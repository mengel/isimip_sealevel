# this only works for hadgem2 ocean data on a 0.5x0.5 isimip grid.

import os
# import xarray as xr
import numpy as np
import functools
# import sys
import netCDF4 as nc
import glob
import shutil
# import matplotlib.pylab as plt
import settings

input_file = settings.gcm_pattern_path+"HadGEM2-ES/zos_Omon_HadGEM2-ES_hist+rcp60_r1i1p1.mergetime.yearmean.remapped.nc4"
#input_file = "/p/projects/tumble/mengel/isimip/20161123_IsimipThermalExpansionOn0p5Grid/HadGEM2-ES/zos_Omon_HadGEM2-ES_rcp60_r1i1p1.mergetime.yearmean.remapped.nc4"

iso_seas_file = input_file[0:-3]+"iso_seas.nc4"
shutil.copyfile(input_file,iso_seas_file)

ncf = nc.Dataset(iso_seas_file,"a")

data_time0 = ncf.variables["zos"][0,:,:]

# define isolated seas and straight of gibraltar
mediterranean_black_sea = np.zeros(data_time0.shape,dtype=bool)
# mediterranean_black_sea.dtype = np.bool
mediterranean_black_sea[90:120,357:437] = True
black_sea = np.zeros(data_time0.shape,dtype=bool)
black_sea[86:100,414:444] = True

mediterranean = mediterranean_black_sea & np.logical_not(black_sea)
# in HadGem2, isololated somehow have a sealevel > 2m. we use it here to mask
other_isolated_seas = (data_time0 > 2) & np.logical_not(mediterranean)
strait_of_gibraltar = np.zeros(data_time0.shape,dtype=bool)
# strait_of_gibraltar[90:120,357:437] = True
strait_of_gibraltar[104:114,340:345] = True

zos = ncf.variables["zos"][:]

zos.mask = zos.mask + other_isolated_seas[np.newaxis,:,:]
saved_mask = zos.mask.copy()

for i in np.arange(zos.shape[0]):
    zos[i,mediterranean] = zos[i,strait_of_gibraltar].mean()
zos.mask = saved_mask

ncf.variables["zos"][:] = zos
ncf.comment = "matthias.mengel@pik masked isolated seas and wrote strait of gibraltar values into mediterranean."
ncf.close()

print "wrote", iso_seas_file
