
# coding: utf-8

# ### Simple Example for sea-level rise projections following the methodology of Mengel et al., PNAS (2016).
# 
# Please cite if you make use of this code.
# This notebook follows `src/create_projections.py`, which was used for the probalistic sea level projections in the paper. We here demonstrate a slightly simplier version without global mean temperature (gmt) sampling, as the gmt ensemble is not openly available.

# In[ ]:

get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_formats = {'svg',}")


# In[ ]:

import matplotlib.pylab as plt
import pickle
import numpy as np
import dimarray as da
import glob
import netCDF4 as nc
import pandas as pd
import datetime
import cdo
cdo = cdo.Cdo()

import sys
sys.path.append("../src")
import get_external_data as gd; reload(gd)

plt.rcParams['figure.figsize'] = 6,10

models = ["IPSL-CM5A-LR","GFDL-ESM2M","MIROC5","HadGEM2-ES"]

## derive_total_slr for the generation of that data.
projection_data = pickle.load(open("../data/projection/projected_slr_1000samples.pkl"))

def get_percentiles(data,percentiles):

    slr_perc = {}
    for perc in percentiles:
        slr_perc[perc] = da.DimArray(np.percentile(
            data.values,perc,axis=1),
            axes=data.time,dims="time")
    return slr_perc


def an(darray):
#     return darray - darray[1986:2005].mean(axis=0)
    return darray - darray[2000]


# In[ ]:

### Glaciers and ice caps

cols = {"rcp26":"#0000ff","rcp60":"#ffa500"}
contrib = "gic"

plt.subplots_adjust(hspace=0.)
axs = {}
for i,m in enumerate(models):
    axs[m] = plt.subplot(4,1,i+1)
    

for run in projection_data:
    model,scen = run.split("_")
    if scen == "rcp85": continue
        
    ax = axs[model]
    d = an(projection_data[run][contrib])
    perc = get_percentiles(d,[5,50,95])
    ax.plot(d.time,perc[50],label = scen,color=cols[scen],lw=2)
    ax.fill_between(d.time,perc[5],perc[95],color=cols[scen],alpha=0.2,lw=0)
    ax.text(0.1,0.8,model,transform=ax.transAxes,
            fontdict={'family': 'sans-serif', 'weight': 'bold', "size": 12})

for x in axs:
    axs[x].set_ylim(-0.04,0.25)
    axs[x].set_xlim(1850,2300)
    axs[x].set_ylabel("Sea level rise in m")

    if x != "HadGEM2-ES":
        axs[x].set_xticklabels("")
        
axs["HadGEM2-ES"].set_xlabel("Time in years")
l1 = axs["GFDL-ESM2M"].legend(loc="upper right")
l1.draw_frame(0)

now = datetime.datetime.now().strftime('%Y%m%d')
#plt.savefig("../figures/glaciers_"+now+".pdf")


# In[ ]:

### Greenland SMB and SID (combined per Monte Carlo)

plt.subplots_adjust(hspace=0.)
axs = {}
for i,m in enumerate(models):
    axs[m] = plt.subplot(4,1,i+1)
    

for run in projection_data:
    model,scen = run.split("_")
    if scen == "rcp85": continue
    
    d = an(projection_data[run]["gis"])
    
    ax = axs[model]
    perc = get_percentiles(d,[5,50,95])
    ax.plot(d.time,perc[50],label = scen,color=cols[scen],lw=2)
    ax.fill_between(d.time,perc[5],perc[95],color=cols[scen],alpha=0.2,lw=0)
    ax.text(0.1,0.8,model,transform=ax.transAxes,
            fontdict={'family': 'sans-serif', 'weight': 'bold', "size": 12})

for x in axs:
    axs[x].set_ylim(-0.14,1)
    axs[x].set_xlim(1850,2300)
    axs[x].set_ylabel("Sea level rise in m")

    if x != "HadGEM2-ES":
        axs[x].set_xticklabels("")
        
axs["HadGEM2-ES"].set_xlabel("Time in years")   
l1 = axs["GFDL-ESM2M"].legend(loc="upper right")
l1.draw_frame(0)

now = datetime.datetime.now().strftime('%Y%m%d')
#plt.savefig("../figures/greenland_"+now+".pdf")


# In[ ]:

### Antarctic SMB and SID (combined per Monte Carlo)

plt.subplots_adjust(hspace=0.)
axs = {}
for i,m in enumerate(models):
    axs[m] = plt.subplot(4,1,i+1)
    

for run in projection_data:
    model,scen = run.split("_")
    if scen == "rcp85": continue
    
    d = an(projection_data[run]["ant"])
    #d = d - d[1986:2005].mean(axis=0)
    ax = axs[model]
    perc = get_percentiles(d,[5,50,95])
    ax.plot(d.time,perc[50],label = scen,color=cols[scen],lw=2)
    ax.fill_between(d.time,perc[5],perc[95],color=cols[scen],alpha=0.2,lw=0)
    ax.text(0.1,0.8,model,transform=ax.transAxes,
            fontdict={'family': 'sans-serif', 'weight': 'bold', "size": 12})

for x in axs:
    axs[x].set_ylim(-0.04,0.3)
    axs[x].set_xlim(1850,2300)
    axs[x].set_ylabel("Sea level rise in m")

    if x != "HadGEM2-ES":
        axs[x].set_xticklabels("")
        
axs["HadGEM2-ES"].set_xlabel("Time in years")
l1 = axs["GFDL-ESM2M"].legend(loc="upper right")
l1.draw_frame(0)

now = datetime.datetime.now().strftime('%Y%m%d')
#plt.savefig("../figures/antarctica_"+now+".pdf")


# In[ ]:

## Thermal expansion

## see get_thermexp_trend.ipynb
trends = {
 'GFDL-ESM2M': np.array([  8.34096029e-05,  -1.56161056e-01]),
 'HadGEM2-ES': np.array([  1.08899115e-04,  -1.97210487e-01]),
 'IPSL-CM5A-LR': np.array([  1.87217867e-05,  -3.02217937e-02]),
 'MIROC5': np.array([  2.85762090e-04,  -5.59582967e-01])}


def get_year(fl):

    """ a cdo hack, which originally returns a list of strings """

    return np.array(cdo.showyear(input = fl)[0].split(),dtype=np.int)


plt.subplots_adjust(hspace=0.)
axs = {}
for i,m in enumerate(models):
    axs[m] = plt.subplot(4,1,i+1)
    

for m in models:
    ax = axs[m]
            
    ax.text(0.1,0.8,m,transform=ax.transAxes,
            fontdict={'family': 'sans-serif', 'weight': 'bold', "size": 12})


    for s in ["rcp26","rcp60"]:
        f = glob.glob("../data/cmip5_input/zostoga_Omon_"+m+"_"+s+"_merged_ts.nc")[0]
        #print f
        ncf = nc.Dataset(f)
        tm = get_year(f)
        zostoga = ncf.variables["zostoga"][:].squeeze()
        zostoga = da.DimArray(zostoga,dims="time",axes=tm)
        fit_fn = np.poly1d(trends[m]) 
        trend = fit_fn (tm) - fit_fn(tm[0])
        detrended = zostoga -  trend
#         ax.plot(d.time,perc[50],label = scen,color=cols[scen],lw=2)
        #print tm.shape,zostoga.shape
        #ax.plot(tm,an(zostoga),label = s,color=cols[s],lw=2,ls="--")
        ax.plot(tm,an(detrended),label = s,color=cols[s],lw=2)
        ncf.close()
    
for x in axs:
    ax = axs[x]
    axs[x].set_ylim(-0.07,0.35)
    axs[x].set_xlim(1850,2300)
    axs[x].set_ylabel("Sea level rise in m")

    ax.set_yticks(ax.get_yticks()[1:-1])
    if x != "HadGEM2-ES":
        axs[x].set_xticklabels("")

axs["HadGEM2-ES"].set_xlabel("Time in years")
l1 = axs["GFDL-ESM2M"].legend(loc="upper right",fontsize=10)
l1.draw_frame(0)

now = datetime.datetime.now().strftime('%Y%m%d')
#plt.savefig("../figures/thermexp_"+now+".pdf")


# In[ ]:

## Thermal expansion, compare detrended to normal and Mengel et al. method.

## see get_thermexp_trend.ipynb
trends = {
 'GFDL-ESM2M': np.array([  8.34096029e-05,  -1.56161056e-01]),
 'HadGEM2-ES': np.array([  1.08899115e-04,  -1.97210487e-01]),
 'IPSL-CM5A-LR': np.array([  1.87217867e-05,  -3.02217937e-02]),
 'MIROC5': np.array([  2.85762090e-04,  -5.59582967e-01])}

plt.subplots_adjust(hspace=0.)
axs = {}
for i,m in enumerate(models):
    axs[m] = plt.subplot(4,1,i+1)
    

for m in models:
    ax = axs[m]
            
    ax.text(0.1,0.8,m,transform=ax.transAxes,
            fontdict={'family': 'sans-serif', 'weight': 'bold', "size": 12})


    for s in ["rcp26","rcp60"]:
        f = glob.glob("../data/cmip5_input/zostoga_Omon_"+m+"_"+s+"_merged_ts.nc")[0]
        #print f
        ncf = nc.Dataset(f)
        tm = get_year(f)
        zostoga = ncf.variables["zostoga"][:].squeeze()
        zostoga = da.DimArray(zostoga,dims="time",axes=tm)
        fit_fn = np.poly1d(trends[m]) 
        trend = fit_fn (tm) - fit_fn(tm[0])
        detrended = zostoga -  trend
#         ax.plot(d.time,perc[50],label = scen,color=cols[scen],lw=2)
        #print tm.shape,zostoga.shape
        ax.plot(tm,an(zostoga),label = s+" nodetrend",color=cols[s],lw=2,ls="--")
        ax.plot(tm,an(detrended),label = s,color=cols[s],lw=2)
        ncf.close()
        
for run in projection_data:
    model,s = run.split("_")
    if s == "rcp85": continue
        
    ax = axs[model]
    d = projection_data[run]["thermexp"]
    perc = get_percentiles(d,[50])
    ax.plot(d.time,an(perc[50]),label = s+" mengel2016",color=cols[s],lw=1)
        
        
for x in axs:
    axs[x].set_ylim(-0.04,0.35)
    axs[x].set_xlim(1850,2300)
    axs[x].set_yticks(axs[x].get_yticks()[1:-1])
    axs[x].set_ylabel("Sea level rise in m")

    if x != "HadGEM2-ES":
        axs[x].set_xticklabels("")

axs["HadGEM2-ES"].set_xlabel("Time in years")
l1 = axs["GFDL-ESM2M"].legend(loc="upper right",fontsize=10)
l1.draw_frame(0)

now = datetime.datetime.now().strftime('%Y%m%d')
#plt.savefig("../figures/thermexp_compare_"+now+".pdf")


# In[ ]:

## non climate driven contributions
fig = plt.figure()
plt.subplots_adjust(hspace=0.)
axs = []

tm = np.arange(1850,2301)

## Groundwater after Wada 2012
ax1 = plt.subplot(312)
ax1.plot(gd.gw_wada.time,an(gd.gw_wada),"k",lw=2)
ax1.fill_between(gd.gw_wada_lower.Years,an(gd.gw_wada_lower),
                 an(gd.gw_wada_upper),
                 color="grey",lw=0,alpha=0.3)

## Natural Glacier contribution after Marzeion et al. 2014
ax2 = plt.subplot(311)
gic_nat = gd.natural_contrib(tm)/1.e3

ax2.plot(gd.nat.time,an(gd.nat)/1.e3,"b",lw=1, label="Marzeion et al. 2014")
ax2.plot(gic_nat.time,an(gic_nat),"k",lw=2, label="quadratic fit")

l1 = ax2.legend(loc="lower right")
l1.draw_frame(0)

ax2.set_xticklabels("")
ax1.set_ylim(-0.03,0.3)
ax2.set_ylim(-0.07,0.027)

ax1.set_xlabel("Time in years")
tlab = ["Land water storage","Natural glacier contribution"]
for i,ax in enumerate([ax1,ax2]):
    ax.set_xlim(1850,2300)
    ax.set_ylabel("Sea level rise in m")
        
    ax.text(0.1,0.85,tlab[i],transform=ax.transAxes,
        fontdict={'family': 'sans-serif', 'weight': 'bold', "size": 12})
    
#plt.savefig("../figures/non-climate-driven.pdf")


# In[ ]:

## Total SLR

## the forcing-independent contributions from LWS and natural glaciers are added here.
## we only add their median estimates, so no uncertainty from these enters total slr for now.

fig = plt.figure()
plt.subplots_adjust(hspace=0.)
axs = {}
for i,m in enumerate(models):
    ax = plt.subplot(4,1,i+1)
    
    ax.text(0.1,0.8,m,transform=ax.transAxes,
        fontdict={'family': 'sans-serif', 'weight': 'bold', "size": 12})
    axs[m] = ax
    
for run in sorted(projection_data):
    model,scen = run.split("_")
    #print run
    if scen == "rcp85": continue
        
    ax = axs[model]
    tot = projection_data[run]["total"]
    perc = get_percentiles(tot,[5,50,95])
    ax.plot(tot[2000:].time,an(perc[50][2000:]),label = scen,color=cols[scen],lw=2)
    ax.fill_between(tot[2000:].time,an(perc[5][2000:]),an(perc[95][2000:]),color=cols[scen],alpha=0.2,lw=0)        
        
for x in axs:
    ax = axs[x]
    ax.plot(gd.kopp_med.index,gd.kopp_med,color="k",lw=2,label="Kopp et al. 2016")
    ax.fill_between(gd.kopp_med.index,gd.kopp_med-gd.kopp_1s,gd.kopp_med-gd.kopp_1s,
                color="grey",alpha=0.3,lw=0)
    
    ax.set_ylim(-0.22,1.8)
    ax.set_xlim(1600,2300)
    ax.set_ylabel("Sea level rise in m")

    if x != "HadGEM2-ES":
        ax.set_xticklabels("")
    
#fig.text(0.06, 0.5, 'Sea level rise in m', ha='center', va='center', rotation='vertical')
axs["HadGEM2-ES"].set_xlabel("Time in years")
l1 = axs["GFDL-ESM2M"].legend(loc="upper right",fontsize=10)
l1.draw_frame(0)

now = datetime.datetime.now().strftime('%Y%m%d')
#plt.savefig("../figures/total_slr_lws_gicnat_"+now+".pdf")

