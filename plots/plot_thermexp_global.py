""" Thermal expansion, taken from the four ISIMIP GCMs directly.
"""

import matplotlib.pylab as plt
import plothelper as ph; reload(ph)
import numpy as np
import glob
import netCDF4 as nc
import dimarray as da

plt.style.use("classic")
plt.rcParams['figure.figsize'] = 6,10

## see get_thermexp_trend.ipynb
trends = {
 'GFDL-ESM2M': np.array([  8.34096029e-05,  -1.56161056e-01]),
 'HadGEM2-ES': np.array([  1.08899115e-04,  -1.97210487e-01]),
 'IPSL-CM5A-LR': np.array([  1.87217867e-05,  -3.02217937e-02]),
 'MIROC5': np.array([  2.85762090e-04,  -5.59582967e-01])}


plt.subplots_adjust(hspace=0.)
axs = {}
for i,m in enumerate(ph.models):
    axs[m] = plt.subplot(4,1,i+1)

for m in ph.models:
    ax = axs[m]

    ax.text(0.1,0.8,m,transform=ax.transAxes,
            fontdict={'family': 'sans-serif', 'weight': 'bold', "size": 12})

    for s in ["rcp26","rcp60"]:
        f = glob.glob("../data/cmip5_input/zostoga_Omon_"+m+"_"+s+"_merged_ts.nc")[0]
        #print f
        ncf = nc.Dataset(f)
        tm = ph.get_year(f)
        zostoga = ncf.variables["zostoga"][:].squeeze()
        zostoga = da.DimArray(zostoga,dims="time",axes=tm)
        fit_fn = np.poly1d(trends[m])
        trend = fit_fn (tm) - fit_fn(tm[0])
        detrended = zostoga -  trend
#         ax.plot(d.time,perc[50],label = scen,color=cols[scen],lw=2)
        #print tm.shape,zostoga.shape
        #ax.plot(tm,an(zostoga),label = s,color=cols[s],lw=2,ls="--")
        ax.plot(tm,ph.an(detrended),label = s,color=ph.cols[s],lw=2)
        ncf.close()

for x in axs:
    ax = axs[x]
    axs[x].set_ylim(-0.07,0.35)
    axs[x].set_xlim(1850,2300)
    axs[x].set_ylabel("Sea level rise in m")

    ax.set_yticks(ax.get_yticks()[1:-1])
    if x != "HadGEM2-ES":
        axs[x].set_xticklabels("")

axs["HadGEM2-ES"].set_xlabel("Time in years")
l1 = axs["GFDL-ESM2M"].legend(loc="upper right",fontsize=10)
l1.draw_frame(0)

ph.save_figure("../figures/thermexp")