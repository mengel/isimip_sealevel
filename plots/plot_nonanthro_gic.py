""" Parametrized version of the non-anthropogenic glacier contribution, following data
    from Marzeion et al., Science (2014)
"""

import sys
import matplotlib.pylab as plt
import numpy as np
import plothelper as ph; reload(ph)

plt.style.use("classic")
plt.rcParams['figure.figsize'] = 6,10

sys.path.append("../isimip_slr")
import get_external_data as gd; reload(gd)

fig = plt.figure(figsize=(6,6))

tm = np.arange(1850,2301)

## Natural Glacier contribution after Marzeion et al. 2014
ax2 = plt.subplot(111)
gic_nat = gd.natural_contrib(tm)/1.e3

ax2.plot(gd.nat.time,ph.an(gd.nat)/1.e3,"b",lw=1, label="Marzeion et al. 2014")
ax2.plot(gic_nat.time,ph.an(gic_nat),"k",lw=2, label="quadratic fit")

l1 = ax2.legend(loc="lower right")
l1.draw_frame(0)

# ax2.set_xticklabels("")
# ax1.set_ylim(-0.03,0.3)
ax2.set_ylim(-0.07,0.027)

tlab = ["Land water storage","Natural glacier contribution"]
# for i,ax in enumerate([ax1,ax2]):
ax2.set_xlim(1850,2300)
ax2.set_ylabel("Sea level rise in m")
ax2.set_xlabel("Time in years")
ax2.text(0.1,0.85,"Non-anthropogenic\nglacier contribution",transform=ax2.transAxes,
    fontdict={'family': 'sans-serif', 'weight': 'bold', "size": 12})

ph.save_figure("../figures/gic_nonanthro")