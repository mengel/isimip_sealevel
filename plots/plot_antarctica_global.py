""" Antarctic SMB and SID (combined per Monte Carlo)
"""

import matplotlib.pylab as plt
import plothelper as ph; reload(ph)

plt.style.use("classic")
plt.rcParams['figure.figsize'] = 6,10

plt.subplots_adjust(hspace=0.)
axs = {}
for i,m in enumerate(ph.models):
    axs[m] = plt.subplot(4,1,i+1)


for run in ph.projection_data:
    model,scen = run.split("_")
    if scen == "rcp85": continue

    d = ph.an(ph.projection_data[run]["ant"])
    #d = d - d[1986:2005].mean(axis=0)
    ax = axs[model]
    perc = ph.get_percentiles(d,[5,50,95])
    ax.plot(d.time,perc[50],label = scen,color=ph.cols[scen],lw=2)
    ax.fill_between(d.time,perc[5].values,perc[95].values,color=ph.cols[scen],alpha=0.2,lw=0)
    ax.text(0.1,0.8,model,transform=ax.transAxes,
            fontdict={'family': 'sans-serif', 'weight': 'bold', "size": 12})

for x in axs:
    axs[x].set_ylim(-0.04,0.3)
    axs[x].set_xlim(1850,2300)
    axs[x].set_ylabel("Sea level rise in m")
    axs[x].yaxis.set_ticks_position('both')

    if x != "HadGEM2-ES":
        axs[x].set_xticklabels("")

axs["HadGEM2-ES"].set_xlabel("Time in years")
l1 = axs["GFDL-ESM2M"].legend(loc="upper right")
l1.draw_frame(0)

ph.save_figure("../figures/antarctica")