""" Glaciers and ice caps, the "gic" variable is the combined anthropogenic and non-anthropogenic
    glacier contribution, see project_global_slr.py for details.
"""

import matplotlib.pylab as plt
import plothelper as ph; reload(ph)

plt.style.use("classic")
plt.rcParams['figure.figsize'] = 6,10

contrib = "gic"

plt.subplots_adjust(hspace=0.)
axs = {}
for i,m in enumerate(ph.models):
    axs[m] = plt.subplot(4,1,i+1)

for run in ph.projection_data:
    model,scen = run.split("_")
    if scen == "rcp85": continue

    ax = axs[model]
    d = ph.an(ph.projection_data[run][contrib])
    perc = ph.get_percentiles(d,[5,50,95])
    ax.plot(d.time,perc[50],label = scen,color=ph.cols[scen],lw=2)
    ax.fill_between(d.time,perc[5].values,perc[95].values,color=ph.cols[scen],alpha=0.2,lw=0)
    ax.text(0.1,0.8,model,transform=ax.transAxes,
            fontdict={'family': 'sans-serif', 'weight': 'bold', "size": 12})

for x in axs:
    axs[x].set_ylim(-0.1,0.25)
    axs[x].set_yticks(axs[x].get_yticks()[1:-1])
    axs[x].set_xlim(1850,2300)
    axs[x].set_ylabel("Sea level rise in m")
    axs[x].yaxis.set_ticks_position('both')
    if x != "HadGEM2-ES":
        axs[x].set_xticklabels("")

axs["HadGEM2-ES"].set_xlabel("Time in years")
l1 = axs["GFDL-ESM2M"].legend(loc="upper right")
l1.draw_frame(0)

ph.save_figure("../figures/glaciers")