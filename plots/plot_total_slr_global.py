"""
Total SLR projections

The forcing-independent natural glacier contribution is added before, see project_global_slr.py.
We do not yet incorporate an LWS contribution for the projection as we are waiting for the
Group3 global water experiments from PCR-GLOBWB. This contribution will be added at a later stage
and stated on the ISIMIP website.
"""

import sys
import matplotlib.pylab as plt
import plothelper as ph; reload(ph)

sys.path.append("../isimip_slr")
import get_external_data as gd; reload(gd)

plt.style.use("classic")
plt.rcParams['figure.figsize'] = 6,10


fig = plt.figure()
plt.subplots_adjust(hspace=0.)
axs = {}
for i,m in enumerate(ph.models):
    ax = plt.subplot(4,1,i+1)

    ax.text(0.1,0.8,m,transform=ax.transAxes,
        fontdict={'family': 'sans-serif', 'weight': 'bold', "size": 12})
    axs[m] = ax

for run in sorted(ph.projection_data):
    model,scen = run.split("_")
    #print run
    if scen == "rcp85": continue

    ax = axs[model]
    tot = ph.projection_data[run]["total"]
    perc = ph.get_percentiles(tot,[5,50,95])
    ax.plot(tot[2006:].time,ph.an(perc[50])[2006:],label = scen,color=ph.cols[scen],lw=2)
    ax.fill_between(tot[2006:].time,ph.an(perc[5])[2006:].values,ph.an(perc[95])[2006:].values,
                    color=ph.cols[scen],alpha=0.2,lw=0)

for x in axs:
    ax = axs[x]
    ax.plot(gd.kopp_med.index,gd.kopp_med,color="k",lw=2,label="Kopp et al. 2016")
    ax.fill_between(gd.kopp_med.index,gd.kopp_med-gd.kopp_1s,gd.kopp_med+gd.kopp_1s,
                color="grey",alpha=0.3,lw=0)

    ax.set_ylim(-0.22,1.8)
    ax.set_xlim(1600,2300)
    ax.set_ylabel("Sea level rise in m")

    if x != "HadGEM2-ES":
        ax.set_xticklabels("")

axs["HadGEM2-ES"].set_xlabel("Time in years")
l1 = axs["GFDL-ESM2M"].legend(loc="upper right",fontsize=10)
l1.draw_frame(0)

ph.save_figure("../figures/total_slr")