""" suit of functions and data used throughout the different plot scripts
"""

import os, sys
import pickle
import dimarray as da
import numpy as np
import matplotlib.pylab as plt
sys.path.append("../")
sys.path.append("../isimip_slr")
import get_external_data as gd; reload(gd)
import config
import datetime
import cdo
cdo = cdo.Cdo()

models = ["IPSL-CM5A-LR","GFDL-ESM2M","MIROC5","HadGEM2-ES"]
cols = {"rcp26":"#0000ff","rcp60":"#ffa500"}


projection_data_path = os.path.join(
    config.outdatapath,"globslr","projected_slr_"+
    str(config.nrealizations)+"samples.pkl")

projection_data = pickle.load(open(projection_data_path))

def get_percentiles(data,percentiles):

    slr_perc = {}
    for perc in percentiles:
        slr_perc[perc] = da.DimArray(np.percentile(
            data.values,perc,axis=1),
            axes=data.time,dims="time")
    return slr_perc

def an(darray):
#     return darray - darray[1986:2005].mean(axis=0)
    return darray - darray[2005]


def get_year(fl):

    """ a cdo hack, which originally returns a list of strings """

    return np.array(cdo.showyear(input = fl)[0].split(),dtype=np.int)

def save_figure(name):

    """ only use this from command line, does not work within
    ipython without an extra plt.clf() """

    now = datetime.datetime.now().strftime('%Y%m%d')
    for fm in [".png",".pdf"]:
        plt.savefig(name+"_"+now+fm, bbox_inches='tight', dpi=200)
