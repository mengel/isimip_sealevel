import os
import xarray as xr
import numpy as np
import functools
import sys
import netCDF4 as nc
# if "../" not in sys.path: sys.path.append("../")
import isimip_slr.regional as regio; reload(regio)
import settings
# basedatadir = "/home/mengel/data/"

fingerprints = regio.get_fingerprints(settings.fingerprint_path)

for model in settings.models:

    for scenario in settings.scenarios:

        print "##",model,scenario
        outfile = os.path.join(
            settings.outdatapath,"regslr_"+model+"_"+scenario+"_r1i1p1.nc4")

        globslr = regio.get_global_projections(settings.global_proj_path, model,
            scenario, settings.anomaly_time)

        gcm_pattern, mask = regio.get_gcm_patterns(settings.gcm_pattern_path, model, scenario)

        gmt_time = regio.get_gmt_time(settings.cmip5_tas_path, model, scenario)

        ncfile = regio.initialize_outfile(outfile, settings.global_local_pairs,
                                globslr, settings.percentiles, settings.lons,
                                settings.lats, gmt_time)

        # ncfile = regio.initialize_outfile_xarray(fp_components,
        #                         globslr, percentiles, lons, lats)

        regio.calc_and_write_fingerprints(ncfile, settings.percentiles, settings.regio_sample_size,
                                          settings.fp_components, settings.global_local_pairs, globslr,
                                          fingerprints, mask, chunks=50)

        # ensure that gcm_anom time complies with anom time of other timeseries.
        regio.add_dynamic_thermosteric(ncfile, settings.percentiles,
                                       settings.global_local_pairs, globslr,
                                       gcm_pattern, anom_time = settings.anomaly_time)

        ncfile.close()
