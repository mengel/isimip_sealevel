import os
import numpy as np
import netCDF4 as nc
import glob
import xarray as xr
import cdo
import functools
import subprocess
import datetime
from collections import OrderedDict

cdoo = cdo.Cdo(debug=False)

# this is the fixed ISIMIP2 grid.
lons = np.arange(-179.75, 179.75+0.5,0.5)
lats = np.arange(-89.75, 89.75+0.5,0.5)

def cdo_get_year(ncfile):

    """ a cdo hack, which originally returns a list of strings """

    return np.array(
        cdoo.showyear(input = ncfile)[0].split(),dtype=np.int)


def shift_data_by_180deg(lons,data):

    """ shift the input data by -180 degrees, so that a field with
        lon=0..360 is aligned on -180..180 """

    shuffle = np.tile(lons < 180.,(360,1))
    data_shuffled = data.copy()
    data_shuffled[shuffle] = data[~shuffle]
    data_shuffled[~shuffle] = data[shuffle]
    return data_shuffled


def get_global_projections(global_proj_path, model, scenario, anomaly_time):

    """get the global slr projections created for ISIMIP2,
    based on Mengel et al. 2016. """

    fpath =os.path.join(global_proj_path, model+"_"+scenario+".nc")
    try:
        globslr = xr.open_dataset(fpath,  decode_times=False)
    except IOError:
        print fpath, "not found."
        raise

    for var in globslr.variables.keys():
        if var not in ["time","sample"]:
            globslr[var] = globslr[var] - globslr[var].loc[anomaly_time]

    return globslr


def get_fingerprints(fingerprint_path):

    """ get the fingerprint data for glaciers and ice sheets.
        they do not depend on scenario and model.
        check if longitudes and latitudes are as expected. data
        should come from a 00 ..360 lon grid and is shifted to a
        -180 ... 180 lon grid"""


    fp_names = ["grl", "ant", "ant_west", "ant_east", "glaciers_rcp45", "glaciers_rcp85"]

    fps = {}

    for f in fp_names:

        fpath = glob.glob(fingerprint_path+"/*"+f+"*nc")[0]
        try:
            dataset = xr.open_dataset(fpath)
        except IOError:
            print fpath, "not found."
            raise

        dataset.rename({"x":"lon","y":"lat"}, inplace=True)
        ## ensure that the data comes from a lon 0..360 grid
        fp_lons = dataset["lon"][:]
        assert np.all(lons == fp_lons - 180)
        assert np.all(lats == dataset["lat"][:])

        fps[f] = dataset["RSL"]
        fps[f][:] = shift_data_by_180deg(fp_lons,dataset["RSL"].values)

        ## make the fingerprint "unit free", so it just can be multiplied
        ## with the global slr and have global slr units
        fps[f] = fps[f] * 1000.

    return fps


def get_gcm_patterns(gcm_pattern_path, model, scenario):

    """ get the sea level patterns (zos), which reflect
        regional sea level rise distribution due to density
        and ocean current changes. needs to be added to the thermosteric
        global timeseries. """

    fname = "zos_Omon_"+model+"_hist+"+scenario+"_r1i1p1.mergetime.yearmean.remapped.nc4"
    if model=="HadGEM2-ES":
        fname = "zos_Omon_"+model+"_hist+"+scenario+"_r1i1p1.mergetime.yearmean.remapped.iso_seas.nc4"
    fpath = os.path.join(gcm_pattern_path,model,fname)
    try:
        dataset = xr.open_dataset(fpath)
    except IOError:
        print fpath, "not found."
        raise

    ## lats are 89.75 .. -89.75, flip them
    dataset.coords["lat"] = dataset["lat"][::-1]

    assert np.all(lons == dataset["lon"][:])
    assert np.all(lats == dataset["lat"][:])

    # convert time to years
    dataset["time"] = cdo_get_year(fpath)
    ## for easier combination with the global data, delete specific time bounds
    del dataset["time_bnds"]
    del dataset["bnds"]
    # # flip also the data along latitudes
    dataset["zos"][:] = dataset["zos"][:,::-1,:]
    # FIXME: do we need to take the anomaly to 2000 to get sea level CHANGE?

    # get the land mask from timestep zero, MIROC5 does not have nans, use custom value.
    if model == "MIROC5":
        mask = np.isclose(dataset["zos"][0,:,:],0.00024337)
    else:
        mask = np.isnan(dataset["zos"][0,:,:].values)

    return dataset, mask


def get_gmt_time(directory, model, scenario):

    fl = os.path.join(directory,model+"_"+scenario+"_merged_ts.nc4")
    gmt_time = {}
    ncf = nc.Dataset(fl,"r")
    gmt_time["values"] = ncf.variables["time"][:]
    gmt_time["units"] = ncf.variables["time"].units
    gmt_time["calendar"] = ncf.variables["time"].calendar
    ncf.close()

    return gmt_time


def initialize_outfile(outfile, global_local_pairs,
                        globslr, percentiles, lons, lats, gmt_time):

    print "## initialize", outfile
    ncout = nc.Dataset(outfile,"w")
    ncout.createDimension('time',size=len(globslr["glacier"].time))
    ncout.createDimension('percentile',size=len(percentiles))
    ncout.createDimension('lat',size=len(lats))
    ncout.createDimension('lon',size=len(lons))

    nct   = ncout.createVariable( 'time','float32',('time',) )
    ncp   = ncout.createVariable( 'percentile','float32',('percentile',) )
    nclt   = ncout.createVariable( 'lat','float32',('lat',) )
    ncln   = ncout.createVariable( 'lon','float32',('lon',) )

    nct[:] = gmt_time["values"]
    nct.units = gmt_time["units"]
    nct.calendar = gmt_time["calendar"]
    nct.axis = "T"
    ncp[:] = percentiles
    ncp.axis = "Z"
    nclt[:] = lats
    nclt.axis = "Y"
    ncln[:] = lons
    ncln.axis = "X"


    for contrib in global_local_pairs:
        vr = ncout.createVariable(
                contrib,'float32',('time','percentile','lat','lon'), zlib=True,
                fill_value = 1.e20)
        vr.units = "m"

    vr = ncout.createVariable(
            'total','float32',('time','percentile','lat','lon'), zlib=True,
                fill_value = 1.e20)
    vr.units = "m"

    vr = ncout.createVariable(
        'ocean','float32',('time','percentile','lat','lon'), zlib=True,
                fill_value = 1.e20)
    vr.units = "m"

    ncout.description = "Regional sea level rise projections for ISIMIP2."
    ncout.method ="Mengel et al. PNAS (2016) and Bamber and Riva (2010)"
    ncout.cite = "http://www.geosci-model-dev-discuss.net/gmd-2016-229/"
    ncout.created_by = "matthias.mengel@pik-potsdam.de"

    githash = subprocess.check_output(["git", "rev-parse", "HEAD"])
    ncout.githash =  githash[0:20]
    now = datetime.datetime.now().strftime("%B %d, %Y")
    ncout.creation_date = now

    return ncout


# @profile
def calc_and_write_fingerprints(ncfile, percentiles, regio_sample_size,
                                fp_components,
                                global_local_pairs, globslr, fingerprints, mask, chunks=5):

    indices = {}
    for i,c in enumerate(fp_components):
        np.random.seed(i)
        indices[c] = np.random.randint(0,globslr.sample.size,regio_sample_size)

    for tar in np.array_split(np.arange(globslr.time.size),chunks):
    # for t in np.arange(globslr.time.size)[:]:
        print tar
        reg_slr_tot_at_t = np.zeros([len(tar),regio_sample_size,len(lats),len(lons)])
        for i,c in enumerate(fp_components):

            regslr_comp_at_t = globslr[c][tar,indices[c]]*fingerprints[global_local_pairs[c]]
            for ip,p in enumerate(percentiles):
                perc = np.percentile(regslr_comp_at_t,p,axis=1)
                # mask the land, do it after percentile calculus because faster
                perc[:,mask] = 1e20
                ncfile[c][tar,ip,:,:] = perc

            reg_slr_tot_at_t += regslr_comp_at_t

        for ip,p in enumerate(percentiles):
            ncfile['total'][tar,ip,:,:] = np.percentile(reg_slr_tot_at_t,p,axis=1)


def add_dynamic_thermosteric(ncfile, percentiles, global_local_pairs, globslr,
                             gcm_pattern, anom_time = 2000):

    """ combine the global thermosteric expansion and the dynamic ocean pattern
        to create the ocean slr contribution.
        CHECK: anomaly times for dynamic slr are one fixed year.
        it should be made sure, that this is always consistent with the
        glob slr anomaly period. """


    ## use xarrays broadcasting rule here.
    gcm_available_time = np.searchsorted(globslr["steric_gcm"].time, gcm_pattern.time)
    gcm_unavailable_time = np.searchsorted(globslr["steric_gcm"].time,
                            np.setxor1d(globslr["steric_gcm"].time,gcm_pattern.time))

    ocean_contribution = (gcm_pattern["zos"] -
                          gcm_pattern["zos"].loc[anom_time,:,:].mean() +
                          globslr["steric_gcm"])

    for i,p in enumerate(percentiles):
        ncfile["ocean"][gcm_available_time,i,:,:] = ocean_contribution.values
        ncfile["total"][gcm_available_time,i,:,:] += ocean_contribution.values
        ncfile["ocean"][gcm_unavailable_time,i,:,:] = 1.e20
        ncfile["total"][gcm_unavailable_time,i,:,:] = 1.e20




# def calc_and_write_fingerprints(ncfile, percentiles, global_local_pairs,
#                                 globslr, fingerprints, mask, chunks = 5):

#     """ combine the global slr projections for glaciers and the ice sheets
#         with the fingerprints. this makes use of xarray broadcasting.
#         calculate and write the data in chunks to netcdf. this is needed to
#         save memory.
#         As we do not have statistics for the fingerprints, this "early percentiling"
#         should be fine.
#     """

#     # regional_slr = {}
#     for contrib in global_local_pairs:

#         print contrib,
#         fingerprint = fingerprints[global_local_pairs[contrib]]

#         for i,perc in enumerate(percentiles):

#             tm = globslr[contrib].time

#             for ar in np.array_split(tm,chunks):

#                 t_ind = np.searchsorted(tm,ar)

#                 # xarray has no better way of percentile calculation implemented,
#                 # so here is the workaround:
#                 globcont = globslr[contrib].loc[ar].reduce(
#                     functools.partial(np.percentile, q=perc), dim='sample')

#                 # use automatic xarray broadcasting to combine timeseries with pattern.
#                 fp = np.ma.masked_array((globcont * fingerprint).values,
#                         mask=np.broadcast_to(mask,(len(ar),mask.shape[0],mask.shape[1])))
#                 ncfile.variables[contrib][t_ind,i,:,:] = fp

#     print ""
#     ## write netcdf attributes here
#     ## git hash for example.

def initialize_outfile_xarray(fp_components,
                        globslr, percentiles, lons, lats):

    """ not used """

    reg_slr = {}
    for c in fp_components+["total","ocean"]:
        reg_slr[c] = xr.DataArray(np.zeros([
                globslr.time.size,len(percentiles),len(lats),len(lons)]))

    ds = xr.Dataset({c: (['time', 'percentile', 'lat', 'lon'], reg_slr[c]) for c in reg_slr},
                     coords={'time': globslr.time,
                             'percentile':percentiles,
                             'lon': lons,
                             'lat': lats,
                             'reference_time': "years since 1861"})
    return ds
