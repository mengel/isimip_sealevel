""" we here collect data not covered by our sea level model and preprocess them
to a common format.
"""

import os
import numpy as np
import dimarray as da
import pandas as pd
#import matplotlib.pylab as plt
from scipy.optimize import curve_fit

# this is needed to be able to run get_external_data
# from different locations and be cross-platform and jupyter compatible.
project_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

##### Kopp et al. 2016, PNAS #####
gslfile_kopp = project_dir+"/data/input/pnas.1517056113.sd03.gsl_ml21.csv"
kopp_gsl = pd.read_csv(gslfile_kopp,header=2,index_col=0)
kopp_gsl = kopp_gsl.reindex(range(1600, 2010))
kopp_gsl = kopp_gsl.interpolate()
kopp_gsl = kopp_gsl.loc[1600:2005]/1.e3
kopp_med = kopp_gsl["mm"] # the median
kopp_1s = kopp_gsl["1s"] # the 1 sigma standard deviation


##### Groundwater after Wada et al 2012 GRL #####
# TODO: maybe update with 80% fraction from Wada 2016

gw_wada = project_dir+"/data/input/TWS_1900-2100_YWADA_UU.csv"
## in m sea level
gw = pd.read_csv(gw_wada,header=0,index_col=0)/1.e3
gw.index.name = "Years"
gw_tot = gw["Cumulative net contribution (total)"]
gw_1s = gw["Cumulative net contribution: uncertainty range"]

def gw_lin(time,a,b):
    """ assume a continuing linear trend after 2100 """
    return a*time+b

gw_tot_2050 = gw_tot.loc[2050:]
popt, pcov = curve_fit(gw_lin, gw_tot_2050.index, gw_tot_2050)


tm = np.arange(1850,2301,1)
## we assume zero lws contribution between 1850 and 1900
## as we have no data.
gw_wada = da.DimArray(np.zeros_like(tm,dtype=np.float),
                 axes=tm,dims="time")
gw_wada[1900:2075] = gw_tot

gw_wada[2075:] = gw_lin(np.arange(2075,2301,1),*popt)

gw_wada_lower = gw_tot - gw_1s
gw_wada_upper = gw_tot + gw_1s
gw_wada_lower = da.from_pandas(gw_wada_lower)
gw_wada_upper = da.from_pandas(gw_wada_upper)

##### Natural glacier contribution #####
# Marzeion et al 2014, antropogenic fraction of glaciers.
# http://www.sciencemag.org/content/345/6199/919.abstract
# updated data, in mm
inputdatadir = project_dir+"/data/input/glaciers_marzeion14/"

natfile = inputdatadir + "global_cumulative_mass_loss_nat_rgi_v4.txt"
marzeion_gic_nat_up = np.loadtxt(natfile, skiprows=2)
models = np.genfromtxt(natfile, skip_header=1, dtype=None)[0, 1:]
marzeion_gic_nat_up = da.DimArray(marzeion_gic_nat_up[:, 1:],
                                  axes=[marzeion_gic_nat_up[:, 0], models], dims=["time", "model"])

nat = marzeion_gic_nat_up.mean(axis=1)

def gicnat(t, a, b):
     return a*(t-2056)**2.+b

## fitting, for reference
# popt2, pcov2 = curve_fit(gicnat, nat.time, nat.values,maxfev=10000)
# print popt2

def natural_contrib(t):
    """ assuming that 2056 zero natural contribution will be reached"""

    gic_nat = da.DimArray(np.zeros_like(t,dtype=np.float),dims="time",axes=t)
    gic_nat[:] = -1.6e-03*(t-2056)**2.
    gic_nat[2056:] = 0.
    return gic_nat
