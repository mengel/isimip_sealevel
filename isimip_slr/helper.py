import os, glob, sys
import numpy as np
import matplotlib.pylab as plt
import netCDF4 as nc
import dimarray as da
import cPickle as pickle
import datetime

# import cdo
# cdo = cdo.Cdo()


## define 1951-1980 to preindustrial (1850-1860)
## global temperature increase based on hadCrut v4.0 data
## see src/get_gmt_data.py for calculation
preind_to_1951_1980 = 0.2640


def get_isimip_gmts(datapath):
    tas_data = {}
    for f in glob.glob(datapath+"/*_merged_ts.nc4"):
        #print f
        ncf = nc.Dataset(f)
        tm = get_year(f)
        #print tm.shape
        #print f
        label = f.split("/")[-1].split("_mer")[0]
        tas = ncf.variables["tas"][:].squeeze()
        #print tas.shape

        tasd = da.DimArray(tas,dims="time",axes=tm)

        ## create anomaly to hadcrutv4 1850-1860 mean
        ## which was used throughout the study as "relative to preindustrial"
        tas_data[label] = tasd - tasd[1951:1980].mean() + preind_to_1951_1980

    return tas_data


def get_gmt_time_date(fl):

    ncf = nc.Dataset(fl,"r")
    gmt_time = ncf.variables["time"][:]
    gmt_time_units = ncf.variables["time"].units
    ncf.close()

    return gmt_time, gmt_time_units


def get_year(fl):

    """ a cdo hack, which originally returns a list of strings """

    return np.array(cdo.showyear(input = fl)[0].split(),dtype=np.int)

def get_gmt_time_date(fl):

    ncf = nc.Dataset(fl,"r")
    gmt_time = ncf.variables["time"][:]
    gmt_time_units = ncf.variables["time"].units
    ncf.close()

    return gmt_time, gmt_time_units

def combine_two_contribs(contribs,realizations):

    """ combine the ice sheet SID and SMB contribution
    probabilistically. """

    nrealizations = len(realizations)
    tm = contribs[0].time
    d = da.DimArray(np.zeros([len(tm),nrealizations]),
                    dims=["time","runnumber"],axes=[tm,realizations])
    for rl in realizations:
        r = np.random.randint(nrealizations)
        d0 = contribs[0][:,r]
        r = np.random.randint(nrealizations)
        d1 = contribs[1][:,r]
        d[:,rl] = d0 + d1

    return d

def write_netcdf(filename, pdata, contribs_to_save, relative_to=2000):

    """ save projection data dictionary as netcdf file."""

    ncout = nc.Dataset(filename,"w")
    ncout.createDimension('time',size=None)
    ncout.createDimension('sample',size=len(pdata["total"].runnumber))
    nct   = ncout.createVariable( 'time','float32',('time',) )
    ncr   = ncout.createVariable( 'sample','float32',('sample',) )

    # pick thermal expansion time and runnumber, all contributions have same
    nct[:]     = pdata["total"].time
    ncr[:]     = pdata["total"].runnumber
    nct.units    = "years since 0001-01-01"
    nct.calendar = "360_day"

    for contrib in contribs_to_save:
        if contrib == "steric_gcm" or contrib == "glacier_nonanthro":
            nc_var = ncout.createVariable(contrib,'float32',('time') )
        else:
            nc_var = ncout.createVariable(contrib,'float32',('time','sample') )

        sldata = pdata[contribs_to_save[contrib]] - pdata[contribs_to_save[contrib]][relative_to]
        nc_var[:] = sldata.values
        nc_var.method = "ISIMIP2 GMD (2016)" ;
        nc_var.description = contrib+" projection following Mengel et. al PNAS (2016) and Frieler et al. GMDD (2016)" ;
        nc_var.units = "meter"

    now = datetime.datetime.now().strftime("%B %d, %Y")
    ncout.comment  = "created by matthias.mengel@pik, " + now
    ncout.citation = "Please cite Mengel et al. PNAS (2016) or Frieler et al. GMDD (2016)"
    ncout.relative_to = "all timeseries relative to year "+str(relative_to)
    ncout.close()