#!/bin/env python

""" does not yet run with sbatch, only in parallel on login nodes,
approximately half an hour to finish."""

#SBATCH --job-name=isimip_sealevel
#SBATCH --output=log/multiprocess_%j.out
#SBATCH --error=/log/slurm_error_%j.err
#SBATCH --time=01:00:00
#SBATCH --qos=short
#SBATCH --account=anthroia
#SBATCH --nodes=1
#SBATCH --exclusive

import multiprocessing
import sys
import os
import xarray as xr
import numpy as np
import functools
import netCDF4 as nc
import itertools
import signal
import settings

# necessary to add cwd to path when script run
# by slurm (since it executes a copy)
sys.path.append(os.path.join(os.getcwd(),"isimip_slr"))
import isimip_slr.regional as regio; reload(regio)

# get number of cpus available to job
try:
    ncpus = int(os.environ["SLURM_JOB_CPUS_PER_NODE"])
except KeyError:
    ncpus = multiprocessing.cpu_count()
print "ncpus",ncpus

fingerprints = regio.get_fingerprints(settings.fingerprint_path)

def create_reg_slr(runid):

    model, scenario = runid.split("_")
    print "##",model,scenario
    outfile = os.path.join(
        settings.outdatapath,"regslr_"+model+"_"+scenario+"_r1i1p1.nc4")

    globslr = regio.get_global_projections(settings.global_proj_path, model, scenario,
                                           settings.anomaly_time)

    gcm_pattern, mask = regio.get_gcm_patterns(settings.gcm_pattern_path, model, scenario)

    gmt_time = regio.get_gmt_time(settings.cmip5_tas_path, model, scenario)

    ncfile = regio.initialize_outfile(outfile, settings.global_local_pairs,
                            globslr, settings.percentiles, settings.lons, settings.lats, gmt_time)

    regio.calc_and_write_fingerprints(ncfile, settings.percentiles, settings.regio_sample_size, settings.fp_components,
                                    settings.global_local_pairs, globslr, fingerprints, mask, chunks=50)

    # ensure that gcm_anom time complies with anom time of other timeseries.
    regio.add_dynamic_thermosteric(ncfile, percentiles, settings.global_local_pairs, globslr,
                                   gcm_pattern, anom_time=settings.anomaly_time)

    ncfile.close()


def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)

runids = []
for model, scenario in itertools.product(settings.models, settings.scenarios):
    runids.append(model+"_"+scenario)

# for runid in runids:
#     create_reg_slr(runid)
# create pool of ncpus workers
p = multiprocessing.Pool(8, initializer=init_worker)
p.map(create_reg_slr,runids)

print "all runs finished."