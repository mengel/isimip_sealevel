import os, glob

models = ["GFDL-ESM2M" ,"HadGEM2-ES","IPSL-CM5A-LR","MIROC5"]
scens = ["rcp26","rcp60","piControl","historical"]
scens = ["historical"]

pre = "cdo -f nc4c -z zip -O mergetime "

## "GFDL-ESM2M" is already averaged.
for mod in models[1:]:
    for scen in scens:
        in1 = glob.glob("data/zostoga_Omon_"+mod+"_"+scen+"_*_*nc")
        in1 = " ".join(sorted(in1))

        # in2 = "data/*"+mod+"_"+scen+"_*.mergetime.yearmean.fldmean.nc4"
        out1 = "data/zostoga_Omon_"+mod+"_"+scen
        cmd = pre+in1+" "+out1+".nc"
        print cmd
        os.system("export SKIP_SAME_TIME=1 && "+cmd)
        cmd = "cdo yearmean "+out1+".nc "+out1+"_ym.nc"
        print cmd

        os.system("export SKIP_SAME_TIME=1 && "+cmd)

## for GFDL, just copy the source file
for scen in scens:
    cmd = ("cp -v data/zostoga_Omon_GFDL-ESM2M_"+scen+"_r1i1p1_*.nc "+
            "data/zostoga_Omon_GFDL-ESM2M_"+scen+"_ym.nc")
    os.system(cmd)
