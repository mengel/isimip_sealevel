import os, glob

models = ["GFDL-ESM2M" ,"HadGEM2-ES","IPSL-CM5A-LR","MIROC5"]
scens = ["rcp26","rcp60"]


pre = "cdo -O trend "

for f in glob.glob("data/zostoga_Omon_*_piControl_*ym.nc"):

    base = f[0:-6]
    print base
    ## estimate trend from preindustrial
    cmd = pre+f+" "+base+"_trend_a.nc "+base+"_trend_b.nc"
    os.system(cmd)

pre = "cdo -O subtrend "
for mod in models:
    for scen in scens:
        b = "data/zostoga_Omon_"+mod+"_"+scen
        cmd = pre+b+"_ym.nc "+b+"_trend_a.nc "+b+"_trend_b.nc "+b+"_ym_detrend.nc"
        os.system(cmd)
