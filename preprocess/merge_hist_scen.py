import os

models = ["GFDL-ESM2M","HadGEM2-ES","IPSL-CM5A-LR","MIROC5"]
scens = ["rcp26","rcp60","rcp85"]

pre = "cdo -f nc4c -z zip -O mergetime "

for mod in models:
    for scen in scens:
        in1 = "data/*"+mod+"_historical_*.mergetime.yearmean.fldmean.nc4"
        in2 = "data/*"+mod+"_"+scen+"_*.mergetime.yearmean.fldmean.nc4"
        out = "data/"+mod+"_"+scen+"_merged_ts.nc4"
        cmd = pre+in1+" "+in2+" "+out
        os.system("export SKIP_SAME_TIME=1 && "+cmd)

## remove intermediate timesteps for HADGEM
for scen in ["rcp26","rcp60"]:
    cmd = ("cdo delete,timestep=1,148 data/HadGEM2-ES_"+scen+
            "_merged_ts.nc4 data/HadGEM2-ES_"+scen+"_merged_tsm.nc4")
    os.system(cmd)
    cmd = ("mv data/HadGEM2-ES_"+scen+"_merged_tsm.nc4 data/HadGEM2-ES_"+scen+
            "_merged_ts.nc4")
    os.system(cmd)