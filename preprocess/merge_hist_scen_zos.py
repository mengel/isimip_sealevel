import os

datapath = "/home/mengel/data/20161123_IsimipThermalExpansionOn0p5Grid/"
models = ["GFDL-ESM2M","HadGEM2-ES","IPSL-CM5A-LR","MIROC5"]
scens = ["rcp26","rcp60","rcp85"]

pre = "cdo -O mergetime "

for mod in models:
    for scen in scens:
        mpath = datapath+mod+"/"
        in1 = mpath+"zos_Omon_"+mod+"_historical_*.mergetime.yearmean.remapped.nc4"
        in2 = mpath+"zos_Omon_"+mod+"_"+scen+"_*.mergetime.yearmean.remapped.nc4"
        out = mpath+"zos_Omon_"+mod+"_hist+"+scen+"_*.mergetime.yearmean.remapped.nc4"
        cmd = pre+in1+" "+in2+" "+out
        print "##",cmd
        os.system("export SKIP_SAME_TIME=1 && "+cmd)

# remove intermediate timesteps for HADGEM
# for scen in ["rcp26","rcp60"]:
#     cmd = ("cdo delete,timestep=1,148 data/zostoga_Omon_HadGEM2-ES_"+scen+
#             "_merged_ts.nc data/zostoga_Omon_HadGEM2-ES_"+scen+"_merged_tsm.nc")
#     os.system(cmd)
#     cmd = ("mv data/zostoga_Omon_HadGEM2-ES_"+scen+"_merged_tsm.nc data/zostoga_Omon_HadGEM2-ES_"+scen+
#             "_merged_ts.nc")
#     os.system(cmd)