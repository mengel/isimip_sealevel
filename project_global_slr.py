# %load ../derive_total_slr.py


"""
matthias.mengel@pik-potsdam.de
For the ISIMIP 1.5 vs 2.0 C project:
create total sea level projections based on Mengel et. al for ice sheets and glaciers
and use the direct output from the ISIMIP GCMs for thermal expansion.
For total sea level rise non-climate driven and non-antropogenic components
need to be added.
"""


import os, glob, sys
import numpy as np
import matplotlib.pylab as plt
import netCDF4 as nc
import dimarray as da
import cPickle as pickle
import isimip_slr.helper as h; reload(h)
import isimip_slr.get_external_data as gd; reload(gd)
import config
import cdo
cdo = cdo.Cdo()

sys.path.append(settings.slpath)

try:
    import contributor_functions as cf; reload(cf)
    import sealevel as sl; reload(sl)
except ImportError:
    raise ImportError(
        "cannot find sealevel code, please check if in " +
        settings.slpath)

tas_data = h.get_isimip_gmts("data/cmip5_input/")

## only for mapping the different names of RCP scenarios
nd = {"rcp26":"RCP3PD",'rcp45':"RCP45",'rcp85':"RCP85"}

def an(darray):
    return darray - darray[1986:2005].mean(axis=0)

projection_data = {}

## project SLR based on Mengel et al. (2016) using the ISIMIP gmt projections
for run in tas_data:

    print "##",run
    projection_data[run] = {}
    gmt = tas_data[run]
    proj_period = gmt.time
    for i,contrib_name in enumerate(project_these):

        print contrib_name,
        calibdata = pickle.load(open(calibdatadir+contrib_name+".pkl","rb"))
        pdata = da.DimArray(None, axes=[proj_period,realizations],
                            dims=["time","runnumber"])

        for n in realizations:
            slr,gmt_i = sl.project(gmt,proj_period,calibdata,n+1)
            pdata[:,n] = slr

        projection_data[run][contrib_name] = pdata
    print ""

## Greenland and Antarctica: derive combined SLR probabilistcally
for cont in ["gis","ant"]:
    for run in projection_data:
        sid = projection_data[run][cont+"_sid"]
        smb = projection_data[run][cont+"_smb"]
        projection_data[run][cont] = h.combine_two_contribs([sid,smb],realizations)


for m in models:
    for s in ["rcp26","rcp60"]:

        ## add the thermal expansion from ISIMIP models
        f = glob.glob("data/cmip5_input/zostoga_Omon_"+m+"_"+s+"_merged_ts.nc")[0]
        ncf = nc.Dataset(f)
        tm = h.get_year(f)
        zostoga = ncf.variables["zostoga"][:].squeeze()
        zostoga = da.DimArray(zostoga,dims="time",axes=tm)
        fit_fn = np.poly1d(trends[m])
        trend = fit_fn(tm) - fit_fn(tm[0])
        detrended = zostoga -  trend
        projection_data[m+"_"+s]["thermexp_gcm"] = detrended

        ## add land water storage, which is independet of the climate forcing
        ## we only add the median and neglect lws uncertainty so far.
        # projection_data[m+"_"+s]["lws"] = gd.gw_wada[tm]

        ## add the non-anthropogenic contribution of glaciers, independent of climate forcing
        projection_data[m+"_"+s]["gic_nat"] = gd.natural_contrib(tm)/1.e3
        ## rename
        projection_data[m+"_"+s]["gic_anthro"] = projection_data[m+"_"+s]["gic"]
        ## combine antropogenic and non-antropogenic contribution
        projection_data[m+"_"+s]["gic"] = (an(projection_data[m+"_"+s]["gic_anthro"]) +
            an(projection_data[m+"_"+s]["gic_nat"]))


## total SLR
## add lws directly here to total, not later in the plotting routine.
## gic_anthro and gic_nat have been combined before.
contribs_for_total = ["thermexp_gcm","gic","gis","ant"]

for run in projection_data:

    if "rcp85" in run: continue

    total_slr = da.zeros_like(projection_data[run]["gic"])

    for name in contribs_for_total:
        # sum up all contributions
        single_contrib = an(projection_data[run][name])
        total_slr += single_contrib

    projection_data[run]["total"] = total_slr

fname = config.outdatapath+"/globslr/projected_slr_" + \
    str(config.nrealizations) + "samples.pkl"

if not os.path.exists(os.path.join(config.outdatapath,"globslr")):
    os.makedirs(os.path.join(config.outdatapath,"globslr"))

print "save to pickle."
pickle.dump(projection_data, open(fname, "wb"), protocol=2)


## save these to netcdf files
contribs_to_save = {"total":"total", "glacier":"gic",
                   "glacier_anthro":"gic_anthro", "glacier_nonanthro":"gic_nat",
                    "steric_gcm":"thermexp_gcm",
                    "greenland":"gis", "antarctica":"ant", "greenland_sid":"gis_sid",
                    "greenland_smb":"gis_smb","antarctica_sid":"ant_sid",
                    "antarctica_smb":"ant_smb"}

## save data to a netcdf file for each model and scenario combination.
print "save to netcdf."
for p in projection_data:

    if "rcp85" in p: continue
    fname = config.outdatapath+"/globslr/"+p+".nc"
    h.write_netcdf(fname, projection_data[p], contribs_to_save, relative_to=2000)