""" Generals settings used by several scripts are provided here. """


outdatapath = "/home/mengel/data/20170725_IsimipRegionalSLRProjections/"

# the number of monte carlo samples (less for testing, 10000 in paper)
nrealizations = 10000
