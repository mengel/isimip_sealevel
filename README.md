## Regional Sea level Projections for ISIMIP2b

This package allows to create regional sea level projections, combining the dynamic
sea level patterns of GCMS with the fingerprints of glaciers, ice sheets and
land water storage. Global glacier and ice sheet projections are driven by the
global mean temperature timeseries of the GCM and are combined with respective
time-independent sea level fingerprints. Global sea level projections from
global mean temperature increase are based on the methodology on
[Mengel et al. (2016)](http://www.pnas.org/content/113/10/2597.abstract).

## Usage

Adapt settings in `config.py`.

Get the necessary data and use [preprocess](preprocess) scripts to make
them usable.
Not all data can be supplied with the code as it is not openly available.
Please ask the author if data is missing. After preprocessing, use

`python project_global_slr.py`

`python project_regional_slr.py`

A script for job submission to the pik cluster is also included.

## Dependencies

The [sealevel package](https://github.com/matthiasmengel/sealevel) for global projections, version 1.0.

## Authors

Matthias Mengel

## License
This code is licensed under GPLv3, see the LICENSE.txt.
