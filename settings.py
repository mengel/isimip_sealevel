import numpy as np

##  global slr settings ##
outdatapath = "/home/mengel/data/20180502_IsimipRegionalSLRProjections/"

slpath = "/home/mengel/projects/sealevel/src/"
## the calibration data each sea level contributor is here (as used in paper)
calibdatadir = "/home/mengel/projects/sealevel/data/calibration/"

# choose here which contributions to project (all as in paper)
project_these = ["thermexp","gic","gis_smb","gis_sid","ant_smb","ant_sid"]
models = ["GFDL-ESM2M","HadGEM2-ES","IPSL-CM5A-LR","MIROC5"]
scenarios = ["rcp26","rcp60"]

## see get_thermexp_trend.ipynb
trends = {
 'GFDL-ESM2M': np.array([  8.34096029e-05,  -1.56161056e-01]),
 'HadGEM2-ES': np.array([  1.08899115e-04,  -1.97210487e-01]),
 'IPSL-CM5A-LR': np.array([  1.87217867e-05,  -3.02217937e-02]),
 'MIROC5': np.array([  2.85762090e-04,  -5.59582967e-01])}

# the number of monte carlo samples (less for testing, 10000 in paper)
nrealizations = 1000
realizations = np.arange(nrealizations)

# basedatadir = "/home/mengel/data/"
## regional slr settings fingerprints ##
basedatadir = "/p/projects/tumble/mengel/isimip/"
fingerprint_path = basedatadir+"20161104_SealevelFingerprintsRiva/"
gcm_pattern_path = basedatadir+"20180505_IsimipThermalExpansionOn0p5Grid/"
global_proj_path = basedatadir+"20161123_IsimipRegionalSLRProjections/globslr/"
outdatapath = basedatadir+"20180502_IsimipRegionalSLRProjections/ver4/"
cmip5_tas_path = basedatadir+"20161123_IsimipRegionalSLRProjections/cmip5_tas/"

## the pairs of global timeseries and spatial patterns
## use rcp45 glacier pattern, very similar to the rcp85.
global_local_pairs = {"glacier":"glaciers_rcp45",
                      "greenland":"grl","antarctica_sid":"ant_west",
                      "antarctica_smb":"ant"}

# this is the fixed ISIMIP2 grid.
lons = np.arange(-179.75, 179.75+0.5,0.5)
lats = np.arange(-89.75, 89.75+0.5,0.5)

percentiles = [5,16.6,33,50,66,83.3,95]

regio_sample_size = 100

# reference the gcm dynamic pattern to this year.
# TODO: check that global timeseries are relative to same year. (now by hand)
anomaly_time = 1861

models = ["GFDL-ESM2M","HadGEM2-ES","IPSL-CM5A-LR","MIROC5"]
models = ["HadGEM2-ES"]
scenarios = ["rcp60","rcp26"]
scenarios = ["rcp26"]
fp_components = [u'antarctica_smb', u'greenland', u'antarctica_sid', u'glacier']
