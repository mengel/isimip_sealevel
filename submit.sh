#!/bin/bash
#SBATCH --job-name=isimip_slr
#SBATCH --qos=short
#SBATCH --time=24:00:00
#SBATCH --account=anthroia
#SBATCH --output=./log/slurm_out.out
#SBATCH --error=./log/slurm_error.err
#SBATCH --ntasks=1
#SBATCH --tasks-per-node=1
#SBATCH --profile=energy
#SBATCH --acctg-freq=energy=5
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=mengel@pik-potsdam.de

source /home/mengel/miniconda2/envs/py2a/bin/activate py2a
echo "starting"  > ./log/startinfo
module load cdo
python project_regional_slr.py > ./log/pyinfo
